//
//  main.m
//  calc.test5
//
//  Created by Timofei Harhun on 15.09.14.
//  Copyright (c) 2014 tima. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TIMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TIMAppDelegate class]));
    }
}
