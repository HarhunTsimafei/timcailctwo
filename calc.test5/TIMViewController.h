//
//  TIMViewController.h
//  calc.test5
//
//  Created by Timofei Harhun on 15.09.14.
//  Copyright (c) 2014 tima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TIMViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet UIButton *whenDidTouchCancel;



- (int) myScreen:(double) variable;
- (double) resultOperation:(NSString *) operation;



@end
