//
//  TIMViewController.m
//  calculator.test3
//
//  Created by Timofei Harhun on 28.08.14.
//  Copyright (c) 2014 tima. All rights reserved.
//

#import "TIMViewController.h"

double variableOne = 0, variableTwo = 0;
bool v_t0=NO,cancel=NO,result=NO;
NSString  *number = @"", *operation = @"";

@interface TIMViewController ()

@end


@implementation TIMViewController

- (IBAction)whenUserDidTouchButtonNum:(UIButton*)sender {
    NSString* text = sender.titleLabel.text;
    number = [number stringByAppendingString:(NSString*) text];
    variableOne=[number doubleValue];
    [self myScreen:variableOne ];
}

- (double) resultOperation:(NSString *) operation {
    if ([operation isEqual:@"-"]){
        variableTwo = variableTwo - variableOne;
    } else {
        if([operation isEqual: @"+"]){
            variableTwo += variableOne;
        } else {
            if([operation isEqual: @"/"]){
                variableTwo = variableTwo / variableOne;
            } else {
                if([operation isEqual: @"*"]){
                    variableTwo = variableTwo * variableOne;
                }}}}
    return variableTwo;
}

- (int) myScreen:(double) variable{
    NSString *screen=@"";
    screen=[NSString stringWithFormat:@"%lg", variable];
    self.myLabel.text = screen;
    return 0;
}

- (IBAction)whenUserDidTouchPMMD:(UIButton*)sender{
    NSString* text = sender.titleLabel.text;
    cancel=NO;
    if(!result){
        if (v_t0){
            variableTwo=[ self resultOperation:operation];
            [self myScreen:variableTwo];
        } else {
            v_t0=YES;
            variableTwo=variableOne;
        }}
    
    if([text isEqual:@"X"]){
        operation=@"*";
        variableOne=1;
    } else {
        if([text isEqual:@"÷"]){
            operation=@"/";
            variableOne=1;
        } else {
            if ([text isEqual:@"+"]) {
                operation=@"+";
                variableOne=0;
            } else {
                if ([text isEqual:@"-"]) {
                    operation=@"-";
                    variableOne=0;
                }}}}
    number=@"";
    result=NO;
}

- (IBAction)WhenDidTouchResult:(id)sender {
    variableTwo=[ self resultOperation:operation];
    [self myScreen:variableTwo];
    number=@"";
    cancel=NO;
    result=YES;
}

- (IBAction)wheDidTouchCancel:(id)sender {
    self.whenDidTouchCancel.titleLabel.text =@"…";
    if (cancel){
        variableTwo=0;
        v_t0=NO;
        operation=@"";
    }
    variableOne=0;
    [self myScreen:variableOne];
    number = @"";
    cancel=YES;
}

- (IBAction)whenDidTouchNotPercentDot:(UIButton*)sender {
    cancel=NO;
    NSString* text = sender.titleLabel.text;
    if([text isEqual:@"+/-"]){
        variableOne=[self.myLabel.text doubleValue];
        variableOne=variableOne*(-1);
    } else {
        if ([text isEqual:@"%"]) {
            variableOne=[self.myLabel.text doubleValue];
            variableOne=variableOne / 100;
        } else {
            number = [number stringByAppendingString:@"."];
            variableOne=[number doubleValue];
        }}
    [self myScreen:variableOne ];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.myLabel.textAlignment = NSTextAlignmentRight;
    self.myLabel.text = @"0";
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
