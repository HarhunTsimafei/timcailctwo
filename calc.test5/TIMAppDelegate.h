//
//  TIMAppDelegate.h
//  calc.test5
//
//  Created by Timofei Harhun on 15.09.14.
//  Copyright (c) 2014 tima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TIMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
